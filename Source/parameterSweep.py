"""!
@brief Contains the implementation of a automated hyper-parameter optimizer.

@details A simple random search hyper-parameter optimizer which tests
a given number of randomly selected parameter configurations drawn from
hard-coded distributions. 

@author Colin M. Van Oort
"""

import cae
import numpy
import time

def bubblesRandomizedParameterSearch(numIterations):
	"""!
	Randomly explores the hyper-parameter space, saves the best model
	to a pickle file, and outputs runtime statistics.

	@param numIterations (int) Number of parameter sets to test
	"""
	
	rng = numpy.random.RandomState()
	bestError = numpy.inf
	bestParamList = []
	bestIteration = -1
	
	for i in range(numIterations):
		print("Beginning search iteration %i/%i...\n"%(i, numIterations))

		paramList = [rng.uniform(low=0.0, high=0.1),
					 1.,
					 0.,
					 rng.uniform(low=0.0, high=0.5),
					 .9,
					 10,
					 2*rng.randint(1,7) + 1,
					 40,
					 30,
					 6,
					 [4*4*40, 4*4*40, 4*4*40],
					 '../ModelConfigs/searchModel.pkl',
					 False]

		try:
			error, model = cae.testCAE(learningRate = paramList[0],
									   learningRateDecay = paramList[1],
									   L1Param = paramList[2],
									   L2Param = paramList[3],
									   momentum = paramList[4],
									   batchSize = paramList[5],
									   filterShape = [paramList[6],paramList[6]],
									   numFilters = paramList[7],
									   maxTrainEpochs = paramList[8],
									   numConvLayers = paramList[9],
									   denseLayerSizes = paramList[10],
									   configFile = paramList[11],
									   showFigs = paramList[12])
			
			if error < bestError:
				print("\tNew minimum found on iteration %i with error %f %% \n" %(i, error*100))
				bestError = error
				bestParamList = paramList
				bestIteration = i
				model.saveModel("../ModelConfigs/bestSearchModel.pkl")

		except:
			error, model = [numpy.inf, None]		

		del(model)
		time.sleep(10)

	print("Optimization complete. Best parameter set found on iteration %i, with error %f, and parameters %s" %(bestIteration, bestError*100, str(bestParamList)))


if __name__ == '__main__':
	bubblesRandomizedParameterSearch(15)