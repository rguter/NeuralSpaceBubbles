"""!
@brief Contains functions for re-formatting, augmenting, and packaging density.

@details Contains three functions (reformatBubbleData, processBubbleData,
and packageBubbleData) which handle all of the density pre-processing
(reformatting, centering, augmentation, etc.) for the NeuralSpaceBubbles
project. When attempting to replicate the results of the project, run
the main function of this file or alternatively run reformatBubbleData,
processBubbleData, and packageBubbleData (in that order) individually.

@author Colin M. Van Oort
"""

from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
import pickle
import random


def reformatBubbleData(dataPath = '../density/',
					   inFiles = ['wind_2cr_flatrho_2315_256.fits', 'wind_2cr_flattracer_2315_256.fits'],
					   outFiles = ['density.pkl', 'target.pkl']):
	"""!
	@brief Converts the input density from .fits files into numpy.ndarray
	density structures and stores them in .pkl files for subsequent use.

	@details Reads in raw density and target density which is held in .fits
	files. density is assumed to be in a 3-D cube shape (256, 256, 256),
	and is reformatted into 2-D images by slicing the raw cubes along
	each primary axis (x, y, z). The slices are stored in numpy arrays
	and	written out to pickle files for later pre-processing and augmentation.

	@param dataPath (str) The path from the current location to the density files.

	@param inFiles (list of 2 str objects) Contains the names of the files which hold the training and target density respectively.

	@param outFiles (list of 2 str objects) Contains the names of the output files.
	"""
	for filePair in zip(inFiles, outFiles):
		inPath = dataPath + filePair[0]
		outPath = dataPath + filePair[1]

		flatSlices = np.empty((768, 256, 256))
		sliceCount = 0
		
		with fits.open(inPath) as fitsData:
			fitsData.info()
			print('\n')
			dataArray = fitsData[0].density
			
		# The density cube is sliced into 2-D squares along each axis
		# (x, y, and z) to allow for a greater number of training examples
		print("Beginning x-axis slices...\n")
		for i in range(256):
			flatSlices[sliceCount, :] = dataArray[i, :, :]
			sliceCount += 1

		print("Beginning y-axis slices...\n")
		for j in range(256):
			flatSlices[sliceCount, :] = dataArray[:, j, :]
			sliceCount += 1

		print("Beginning z-axis slices...\n")
		for k in range(256):
			flatSlices[sliceCount, :] = dataArray[:, :, k]
			sliceCount += 1

		# The reformed density is written to a new file for later use
		print("Writing modified density to %s\n" %(outPath))

		with open(outPath, 'wb') as pickleData:
			pickle.dump(flatSlices, pickleData)

	print("\n")


def processBubbleData(dataPath = '../density/',
					  inFiles = ['density.pkl', 'target.pkl'],
					  outFiles = ['densityAug.pkl', 'targetAug.pkl']):
	"""!
	@brief Performs density centering and augmentation.

	@details Reads in the reformatted density and target density from .pkl
	files. Rescales, centers, and augments the density and the target
	before writing both to new pickle files for final packaging.

	** density augmentations are currently commented out since inclusion
	creates memory issues due to the size of the density set after
	augmentation. Also, in the [original paper on batch normalization](https://arxiv.org/abs/1502.03167) 
	it is recommended to not use density augmentation when applying batch 
	normalization (which we currently use to stabilize training). **

	@param dataPath (str) The path from the current location to the density files.

	@param inFiles (list of 2 str objects) Contains the names of the files which hold the reformatted training and target density respectively.

	@param outFiles (list of 2 str objects) Contains the names of the output files.
	"""
	print("Loading files...\n")
	
	densityPath = dataPath + inFiles[0]
	with open(densityPath, 'rb') as pickleFile:
		density = pickle.load(pickleFile)
		print("%s loaded!\n"%(densityPath))

	targetPath = dataPath + inFiles[1]
	with open(inFiles[1], 'rb') as pickleFile:
		target = pickle.load(pickleFile)
		print("%s loaded!\n"%(targetPath))

	assert density.shape == target.shape

	print("Performing density centering...\n")

	print("Statistics before centering:\n")

	print("density: Mean:%G, Median: %G, Max: %G, Min: %G\n"
		  %(np.mean(density), np.median(density), np.max(density), np.min(density)))

	print("target: Mean:%G, Median: %G, Max: %G, Min: %G\n"
		  %(np.mean(target), np.median(target), np.max(target), np.min(target)))

	# If the density or target have negative values, shift both by the same
	# amount so that both are completly positive
	if np.min(density) < 0 or np.min(target) < 0:
		shiftTerm = 2.*min(np.min(density), np.min(target))
		density -= shiftTerm
		target -= shiftTerm

	# Rescale the density to avoid round off error (may be unnecessary)
	# **Testing should be performed on this**
	scalingTerm = min(np.floor(np.min(np.log(density))),
					  np.floor(np.min(np.log(target))))

	if scalingTerm < 0
		density *= 10**(-1*scalingTerm)

	# Take the natural log of the density (used as a "squishing" function)
	density = np.log(density)

	# Divide by the standard deviation (another "squishing" function)
	density /= np.std(density)

	# Shift the density so that the smallest element is 0.0
	density -= np.min(density)

	# Truncate values less than 1.0 to 0.0
	density = np.where(density>1.0, density, 0.0)

	# density centering (standard M.L. technique)
	density /= np.std(density)+np.finfo(np.float32).eps
	density -= np.mean(density)
	
	if scalingTerm < 0:
		target *= 10**scalingTerm
	target = np.log(target)
	target /= np.std(target)
	target -= np.min(target)
	target = np.where(target>1.0, target, 0.0)
	target /= np.std(target)+np.finfo(np.float32).eps
	target -= np.mean(target)
	
	print("Statistics after centering:\n")

	print("density: Mean:%G, Median: %G, Max: %G, Min: %G\n"
		  %(np.mean(density), np.median(density), np.max(density), np.min(density)))

	print("target: Mean:%G, Median: %G, Max: %G, Min: %G\n"
		  %(np.mean(target), np.median(target), np.max(target), np.min(target)))

	# Initialize intermidiate variables and output arrays
	numExamples = density.shape[0]
	numOperations = 0
	nextIndex = numExamples

	outData = np.empty((numExamples*(numOperations+1), 256, 256))
	outFlag = np.empty((numExamples*(numOperations+1), 256, 256))
 
	outData[0:numExamples, :, :] = density
	outFlag[0:numExamples, :, :] = target

	# ** density augmentations are currently commented out since inclusion
	# causes the dataset to become too large to run on the projects current
	# GPU hardware **

	# # Add in perturbed versions of the original density
	# # Reflection perturbations
	# print("Adding reflection perturbations...\n")

	# # Vertical reflections across the center of the image
	# for i in range(density.shape[0]):
	# 	dataExample = density[i, :, :]
	# 	flagExample = target[i, :, :]

	# 	dataExample = np.flipud(dataExample)
	# 	flagExample = np.flipud(flagExample)

	# 	outData[nextIndex, :, :] = dataExample
	# 	outFlag[nextIndex, :, :] = flagExample

	# 	nextIndex +=1

	# # Horizontal reflections across the center of the image
	# for i in range(density.shape[0]):
	# 	dataExample = density[i, :, :]
	# 	flagExample = target[i, :, :]

	# 	dataExample = np.fliplr(dataExample)
	# 	flagExample = np.fliplr(flagExample)

	# 	outData[nextIndex, :, :] = dataExample
	# 	outFlag[nextIndex, :, :] = flagExample

	# 	nextIndex +=1

	# # Reflections across the main diagonal of the image
	# for i in range(density.shape[0]):
	# 	dataExample = density[i, :, :]
	# 	flagExample = target[i, :, :]

	# 	dataExample.T
	# 	flagExample.T

	# 	outData[nextIndex, :, :] = dataExample
	# 	outFlag[nextIndex, :, :] = flagExample

	# 	nextIndex +=1

	# # Additive White Gaussian Noise (AWGN) perturbations
	# print("Adding AWGN perturbations...\n")
	# for i in range(density.shape[0]):
	# 	dataExample = density[i, :, :]
	# 	flagExample = target[i, :, :]

	# 	wgn = np.random.normal(0, 0.1, dataExample.shape)

	# 	dataExample = np.add(dataExample, wgn)
	# 	flagExample = np.add(flagExample, wgn)

	# 	outData[nextIndex, :, :] = dataExample
	# 	outFlag[nextIndex, :, :] = flagExample

	# 	nextIndex +=1

	# # Rotation perturbations
	# print("Adding rotation perturbations...\n")
	# # 90 degree couter-clockwise rotations
	# for i in range(density.shape[0]):
	# 	dataExample = density[i, :, :]
	# 	flagExample = target[i, :, :]

	# 	dataExample = np.rot90(dataExample)
	# 	flagExample = np.rot90(flagExample)

	# 	outData[nextIndex, :, :] = dataExample
	# 	outFlag[nextIndex, :, :] = flagExample

	# 	nextIndex +=1

	# # 180 degree couter-clockwise rotations
	# for i in range(density.shape[0]):
	# 	dataExample = density[i, :, :]
	# 	flagExample = target[i, :, :]

	# 	dataExample = np.rot90(dataExample, 2)
	# 	flagExample = np.rot90(flagExample, 2)

	# 	outData[nextIndex, :, :] = dataExample
	# 	outFlag[nextIndex, :, :] = flagExample

	# 	nextIndex +=1

	# # 270 degree couter-clockwise rotations
	# for i in range(density.shape[0]):
	# 	dataExample = density[i, :, :]
	# 	flagExample = target[i, :, :]

	# 	dataExample = np.rot90(dataExample, 3)
	# 	flagExample = np.rot90(flagExample, 3)

	# 	outData[nextIndex, :, :] = dataExample
	# 	outFlag[nextIndex, :, :] = flagExample

	# 	nextIndex +=1

	print("Scrambling the density...\n")
	# Randomly reorder the density examples so that the perturbed versions
	# are distributed throughout the dataset
	rng_state = np.random.get_state()
	np.random.shuffle(outData)
	np.random.set_state(rng_state)
	np.random.shuffle(outFlag)

	print("Writing augmented density to files...\n\n")
	with open(dataPath + outFiles[0], 'wb') as pickleFile:
		pickle.dump(outData, pickleFile)
		print("")

	with open(dataPath + outFiles[1], 'wb') as pickleFile:
		pickle.dump(outFlag, pickleFile)


def packageBubbleData(dataPath = '../Data/',
					  inFiles = ['densityAug.pkl', 'tracerAug.pkl'],
					  outFile = 'bubbleDataset.pkl'):
	"""!
	@brief Reads in the processed and augmented data from pickle files
	and performs final packaging.

	@details The density and target are split into corresponding training,
	validation, and testing sets. The corresponding pairs are packaged
	into tuples	and then the three tuples are packaged into an array and
	written to a pickle file which may be ingested by a machine learning
	model.

	@param dataPath (str) The path from the current location to the density files.

	@param inFiles (list) Contains the paths to the files which hold the augmented training and target density.

	@param outFile (str) The desired name of the output file.
	"""

	# Read in the augmented training and target density
	densityPath = dataPath + inFiles[0]
	with open(densityPath, 'rb') as pickleFile:
		density = pickle.load(pickleFile)

	targetPath = dataPath + inFiles[1]
	with open(inFiles[1], 'rb') as pickleFile:
		target = pickle.load(pickleFile)

	# Ensure that the density and target objects have the same dimensions
	assert density.shape == target.shape

	# Divide the training and target density into 3 segments each (training, testing, and validation)
	train = int(0.7*density.shape[0])
	test = int(0.15*density.shape[0])
	valid = int(0.15*density.shape[0])

	# Ensure that the partition sizes create a full partitioning of the
	# original density and target objects
	total = train + test + valid
	train += density.shape[0]-total
	total = train + test + valid

	# Create the partitions
	trainData = np.empty((train, density.shape[1], density.shape[2]))
	trainTarget = np.empty((train, density.shape[1], density.shape[2]))

	index = 0
	for example in range(train):
		trainData[example, :, :] = density[example, :, :]
		trainTarget[example, :, :] = target[example, :, :]
		index += 1

	testData = np.empty((test, density.shape[1], density.shape[2]))
	testTarget = np.empty((test, density.shape[1], density.shape[2]))

	for example in range(test):
		testData[example, :, :] = density[example+train, :, :]
		testTarget[example, :, :] = target[example+train, :, :]
		index += 1
		
	validData = np.empty((valid, density.shape[1], density.shape[2]))
	validTarget = np.empty((valid, density.shape[1], density.shape[2]))

	for example in range(valid):
		validData[example, :, :] = density[example+train+test, :, :]
		validTarget[example, :, :] = target[example+train+test, :, :]
		index += 1

	print("Created training set with dimensions %s,\n validation set with dimensions %s,\n and test set with dimensions %s.\nwriting datasets to %s"
		  %(str(trainData.shape), str(validData.shape), str(testData.shape), "'bubbleDataset.pkl'"))
		
	# The partitions are paired as tuples, stored in a list, and pickled
	# into a new file which is ready to be fed into the model
	with open(datapath+outFile, 'wb') as pickleFile:
		pickle.dump([(trainData,trainTarget), (validData,validTarget), (testData,testTarget)], pickleFile)


if __name__ == '__main__':
	reformatBubbleData()

	processBubbleData()

	packageBubbleData()