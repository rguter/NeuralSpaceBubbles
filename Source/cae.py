"""!
@brief This file implements a Convoluational auto-encoder neural network
archtecture.

@details Multiple helper functions have been implemented in addition
to the CAE architecture which handle data manipulation, creation of
visualizations, and more.

@author Colin M. Van Oort
"""

from networkLayers import BatchNormalizationLayer, CAEConvLayer, CAEDeconvLayer, DenseLayer, ReshapeLayer, FlattenLayer
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import sys
import theano
import theano.tensor as T
import timeit


class BubbleCAE(object):
	"""!
	This class implements a convolutional auto-encoder neural network
	architecture which is used by the NeuralSpaceBubbles project to
	identify radiation driven shells in images of molecular clouds.
	"""

	def __init__(self,
				 rng,
				 inputs,
				 filterShape,
				 inputShape,
				 numConvLayers,
				 denseLayerSizes,
				 poolSize = (2, 2),
				 batchNormalization = True):
		"""!
		Constructs the network.

		** Use a filter with odd height and width dimensions, otherwise
		the input and output dimensions of each layer are not identical **
		
		@param rng (numpy.random.RandomState) a random number generator used to initialize weights.

		@param inputs (theano.tensor.dtensor4) symbolic image tensor of shape inputShape.

		@param filterShape (tuple or list of 4 int objects) the desired filter shape (number of feature maps at layer i, num feature maps at layer i-1, filter height, filter width).

		@param inputShape (tuple or list of 4 int objects) the anticipated input shape (batch size, num feature maps, image height, image width).

		@param numConvLayers (int) the number of layers used to construct the encoder and decoder (each will have this many layers).

		@param denseLayerSizes (tuple or list of int objects) the length of this list determines the number of dense layers created, the value of each element determines the size of each dense layer.

		@param poolSize (tuple or list of 4 int objects) the up sampling (de-pooling) factor (# of rows, # of cols).

		@param batchNormalization (bool) switch to turn batch normalization on or off.
		"""

		assert inputShape[1] == filterShape[1]
		assert numConvLayers >= 1
		assert len(denseLayerSizes) >= 1

		self.rng = rng
		self.inputs = inputs
		self.layers = []

		# Alternate filter shapes used in the decoder in order to obtain
		# outputs with the correct dimensions
		filterShape2 = (filterShape[0], filterShape[0],
						filterShape[2], filterShape[3])

		filterShape3 = (filterShape[1], filterShape[0],
						filterShape[2], filterShape[3])


		if batchNormalization:
			normalizeLayer = BatchNormalizationLayer(self.rng,
													 self.inputs,
													 inputShape)

		self.layers.append(normalizeLayer)

		# Assemble the encoder using convolution layers
		for i in range(numConvLayers):
			if i:
				convLayer = CAEConvLayer(self.rng,
										 self.layers[-1].output,
										 filterShape2,
										 self.layers[-1].outputShape,
										 poolSize)

			else:
				if batchNormalization:
					convLayer = CAEConvLayer(self.rng,
											 normalizeLayer.output,
											 filterShape,
											 inputShape,
											 poolSize)
				else:
					convLayer = CAEConvLayer(self.rng,
											 self.inputs,
											 filterShape,
											 inputShape,
											 poolSize)

			self.layers.append(convLayer)

		flattenLayer = FlattenLayer(self.layers[-1].output,
									self.layers[-1].outputShape)

		# Assemble the dense layers
		for i in range(len(denseLayerSizes)):
			if i:
				denseLayer = DenseLayer(self.rng,
										self.layers[-1].output,
										self.layers[-1].size,
										denseLayerSizes[i])
				
			else:
				denseLayer = DenseLayer(self.rng,
										flattenLayer.output,
										flattenLayer.size,
										denseLayerSizes[i])
			
			self.layers.append(denseLayer)				

		# The reshape layer ensures that the data passed to the decoder
		# has the correct dimensions
		reshapeLayer = ReshapeLayer(self.layers[-1].output,
									self.layers[numConvLayers].outputShape)

		# Assemble the decoder using deconvolution layers
		for i in range(numConvLayers):
			if (i==numConvLayers-1):
				deconvLayer = CAEDeconvLayer(self.rng,
											 self.layers[-1].output,
											 filterShape3,
											 self.layers[-1].outputShape,
											 poolSize)

			elif i:
				deconvLayer = CAEDeconvLayer(self.rng,
											 self.layers[-1].output,
											 filterShape2,
											 self.layers[-1].outputShape,
											 poolSize)

			else:
				deconvLayer = CAEDeconvLayer(self.rng,
											 reshapeLayer.output,
											 filterShape2,
											 reshapeLayer.outputShape,
											 poolSize)
			
			self.layers.append(deconvLayer)

		self.params = []
		for layer in self.layers:
			self.params.extend(layer.params)

		# The base of the cost function used during training
		self.finetuneCost = self.layers[-1].mae

		# The error function used for validation and testing
		self.errors = self.layers[-1].relativeError

		self.output = self.layers[-1].output

	def l1Regularization(self):
		"""!
		Calculates the L1 norm of the network weights.
		
		@return (float) The sum of the absolute values of the network weights.
		"""
		output = 0
		for layer in self.layers:
			try:
				output += T.sum(T.abs_(layer.W))

			except:
				pass

			try:
				output += T.sum(T.abs_(layer.Gamma))

			except:
				pass

		return output

	def l2Regularization(self):
		"""!
		Calculates the square of the L2 norm of the network weights.
		
		@return (float) The sum of the square of the values of the network weights.
		"""
		output = 0
		for layer in self.layers:
			try:
				output += T.sum(T.pow(layer.W, 2))
			
			except:
				pass

			try:
				output += T.sum(T.pow(layer.Gamma, 2))

			except:
				pass

		return output/2.

	def displayFilters(self, numConvLayers, numDenseLayers, numFilters):
		"""!
		Visualizes the filters of the network by unstacking them into 
		2-D structures and creating a tiled display using matplotlib.

		** Need to increase the space between tiles **

		@param numConvLayers (int) the number of layers in the encoder/decoder.

		@param numDenseLayers (int) the number of layers connecting the encoder and decoder.

		@param numFilters (int) how many filters to show from each layer.
		"""
		filters = []
		layerNum = 1

		# Grab the filters from the convolution and deconvolution layers
		for i in range(numConvLayers):
			filters.append(self.layers[layerNum].W)
			layerNum += 1

		for j in range(numDenseLayers):
			layerNum += 1

		for k in range(numConvLayers):
			filters.append(self.layers[layerNum].W)
			layerNum += 1

		# Unstack the filters into 2D images
		unstackedFilters = []
		for element in filters:
			filterVal = element.get_value()
			filterShape = filterVal.shape

			unstackingFilters = []
			
			for i in range(filterShape[0]):
				for j in range(filterShape[1]):
					unstackingFilters.append(filterVal[i,j,:,:])

			unstackedFilters.append(unstackingFilters)

		fig,axes = plt.subplots(numConvLayers*2,numFilters)

		for i in range(numConvLayers*2):
			indexList = self.rng.choice(numFilters, size=numFilters, replace=False)

			for j in range(numFilters):
				if i:
					try:
						axes[i, j].axis('off')
						axes[i, j].imshow(unstackedFilters[i][indexList[j]], cmap=plt.get_cmap('Greys'), interpolation="nearest")

					except IndexError:
						axes[i].axis('off')
						axes[i].imshow(unstackedFilters[i][indexList[j]], cmap=plt.get_cmap('Greys'), interpolation="nearest")

				else:
					try:
						axes[i, j].axis('off')
						axes[i, j].imshow(unstackedFilters[i][j], cmap=plt.get_cmap('Greys'), interpolation="nearest")

					except IndexError:
						axes[i].axis('off')
						axes[i].imshow(unstackedFilters[i][j], cmap=plt.get_cmap('Greys'), interpolation="nearest")

		plt.savefig("../Visualizations/filterVis.png", bbox_inches='tight')
		plt.savefig("../Visualizations/filterVis.pdf", bbox_inches='tight')

	def saveModel(self, outFile='caeConfig.pkl'):
		"""!
		Pickles the entire CAE object for later use.

		@param outFile (str) Desired name of the output file.
		"""
		sys.setrecursionlimit(10000)

		with open(outFile, 'wb') as pickleFile:
			pickle.dump(self, pickleFile)

	def loadModel(self, inFile='caeConfig.pkl'):
		"""!
		Reads in a pickle file and loads the CAE object contained in it.

		@param inFile (str) Name of the file to read from.

		@return (CAE object) A previously saved CAE object.
		"""
		with open(inFile, 'rb') as pickleFile:
			model = pickle.load(pickleFile)

		return model
			
	def evaluate(self, numOutputs, inputs, targets, index, batchSize):
		"""!
		Creates and saves a tiled visualization with dimensions (numOutputs, 3)
		where each row contains the original image, the target, and the
		network output when presented with that rows image.

		@param numOutputs (int) Number of rows in the visualization (randomly selected).

		@param inputs (theano.tensor._shared) Shared variable containing the input data.

		@param targets (theano.tensor._shared) Shared variable containing the target data.

		@param index (theano.tensor.scalar) Symbolic variable which represents the index into the dataset.

		@param batchSize (int) Size of the mini-batches to be passed into the model.
		"""
		evaluateModel = theano.function(inputs = [index],
										outputs = self.output,
										givens = {self.inputs: inputs[index*batchSize:(index + 1)*batchSize]})

		indexList = self.rng.choice(np.arange(inputs.get_value().shape[0]//batchSize), size=math.ceil(numOutputs/batchSize), replace=False)
		inputArray = None
		targetArray = None
		outputArray = None
		for i in range(math.ceil(numOutputs/batchSize)):
			modelOut = evaluateModel(indexList[i])

			if i:
				inputArray = np.concatenate((inputArray, inputs.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, : , :, :]))
				targetArray = np.concatenate((targetArray, targets.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, :, :, :]))
				outputArray = np.concatenate((outputArray, modelOut))

			else:
				inputArray = inputs.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, :, :, :]
				targetArray = targets.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, :, :, :]
				outputArray = modelOut

		imageArrays = [inputArray, targetArray, outputArray]
			
		fig, axes = plt.subplots(numOutputs, 3)
		axCount = 0

		for i in range(numOutputs):
			for array in imageArrays:
				axes[i, axCount].imshow(array[i, 0, :, :], cmap=plt.get_cmap('Greys'), interpolation='nearest')

				# Don't draw the axes
				axes[i, axCount].set_frame_on(False)

				# Remove the ticks
				axes[i, axCount].set_xticks([]) 
				axes[i, axCount].set_yticks([]) 

				# Create titles
				if (i == 0) and (axCount == 0):
					axes[i, axCount].set_title('Input')

				elif (i == 0) and (axCount == 1):
					axes[i, axCount].set_title('Target')

				elif (i == 0) and (axCount == 2):
					axes[i, axCount].set_title('Output')

				else:
					pass

				# Create labels
				if axCount == 0:
					axes[i, axCount].set_ylabel('Input %i'%(i+1), labelpad=25, rotation=0)

				axCount += 1
			axCount = 0

		plt.savefig("../Visualizations/comparisonVis.pdf", bbox_inches='tight')
		plt.savefig("../Visualizations/comparisonVis.png", bbox_inches='tight')

		# Write the data arrays out to a pickle file for later use
		with open('../ModelConfigs/modelOutput.pkl', 'wb') as pickleFile:
			pickle.dump([inputArray, targetArray, outputArray], pickleFile)


def load_data(dataset):
	"""!
	Loads the formatted and augmented data from a pickle file, converts
	the data into theano shared variables so that they may be loaded onto
	the GPU, and outputs the shared variables in the same format as they
	were read in (a list of tuples).

	@param dataset (str) the path to the dataset.

	@return (list of 3 tuple objects) Contains theano shared variables [(training data, training target), (validation data, validation target), (testing data, testing target)].
	"""

	with open(dataset, 'rb') as pickleFile:
		# train_set, valid_set, test_set are tuples of numpy.ndarray 
		# which contain the input data and corresponding target data
		train_set, valid_set, test_set = pickle.load(pickleFile)

	def shared_dataset(data_xy, borrow=True):
		"""!
		An internal function which loads the dataset into theano shared
		variables, allowing for utilization of a GPU.

		@param data_xy (tuple or list of length 2) should contain two numpy.ndarray objects which hold data and target information.

		@param borrow (bool) indicates if the shared varaibles should have the borrow option toggled on or off.

		@return (tuple of 2 theano.tensor._shared objects) a pair of shared variables for a (data, target) pair.
		"""
		data_x, data_y = data_xy
 
		# If the data objects do not have a color channel one is added
		# by expanding the dimensions of the array.
		if len(data_x.shape) != 4:
			data_x = np.expand_dims(data_x, axis=1)
			data_y = np.expand_dims(data_y, axis=1)

		shared_x = T._shared(data_x.astype(np.float32), borrow=borrow)
		shared_y = T._shared(data_y.astype(np.float32), borrow=borrow)

		return shared_x, shared_y

	train_set_x, train_set_y = shared_dataset(train_set)
	valid_set_x, valid_set_y = shared_dataset(valid_set)
	test_set_x, test_set_y = shared_dataset(test_set)
	
	return [(train_set_x, train_set_y), (valid_set_x, valid_set_y), (test_set_x, test_set_y)]


def trainCAE(learningRate=0.01,
			learningRateDecay=.2,
			L1Param=0.001,
			L2Param=0.001,
			momentum=0.9,
			batchSize=10,
			filterShape=[5,5],
			numFilters=5,
			maxTrainEpochs=100,
			numConvLayers=3,
			denseLayerSizes=[32*32*10, 100, 32*32*10],
			dataset='../Data/bubbleDataset.pkl',
			configFile=None,
			loadModel=False,
			showFigs=True):
	"""!
	@param learningRate (float) Rescales the gradients during each training epoch, higher values will result in divergence (inability to learn the task) while lower values will cause the model to converge extremely slowly.

	@param learningRateDecay (float) Controls how quickly the learning rate decays with each epoch. The current learning rate schedule is a linear decay given by (learningRate/(1.0 + epoch*learningRateDecay))

	@param L1Param (float) Regularization parameter which controls the strength fo the L1 regularization term.

	@param L2Param (float) Regularization parameter which controls the strength of the L2 regularization term.

	@param momentum (float) Momentum parameter which controls the mixing ratio between the current gradient and the previous gradient.

	@param batchSize (int) Desired mini-batch size.

	@param filterShape (tuple or list 2 int objects) Shape of all filters created for the encoder/decoder layers.

	@param numFilters (int) Number of filters which will be created for each layer of the encoder/decoder.

	@param maxTrainEpochs (int) A stopping parameter, halts training after this many training epochs.

	@param numConvLayers (int) Number of layers which will be used in the construction of the encoder/decoder of the CAE.

	@param denseLayerSizes (list or tuple of int objects) Length of this list determines how many dense layers will be created and the size of each layer is determined by the corresponding value of this list.

	@param dataset (str) Path to the dataset file.

	@param configFile (str) Path to the configuration file. This location is used as both the location from which a previously trained model may be loaded, and the location to which the new model will be saved.

	@param loadModel (bool) Indicates if a previously trained model is to be loaded.

	@param showFigs (bool) Determines if the generated figures are displayed or not.

	@return (list of length 2) Contains the lowest test error and the model which produced it.
	"""

	assert momentum < 1 and momentum >= 0

	datasets = load_data(dataset)

	train_set_x, train_set_y = datasets[0]
	valid_set_x, valid_set_y = datasets[1]
	test_set_x, test_set_y = datasets[2]

	# compute number of mini-batches for training, validation and testing
	nTrainBatches = train_set_x.get_value(borrow=True).shape[0] // batchSize
	nValidBatches = valid_set_x.get_value(borrow=True).shape[0] // batchSize
	nTestBatches = test_set_x.get_value(borrow=True).shape[0] // batchSize

	print('Building model...\n')

	# allocate symbolic variables for the data
	# index to a mini-batch
	index = T.lscalar()  
	# 4-D tensor for the data
	x = T.ftensor4('x')
	# 4-D tensor for the target
	y = T.ftensor4('y')

	# Construct the neural network
	model = BubbleCAE(rng = np.random.RandomState(),
					  inputs = x,
					  filterShape = (numFilters, 1, filterShape[0], filterShape[1]),
					  inputShape = (batchSize, 1, 256, 256),
					  numConvLayers = numConvLayers,
					  denseLayerSizes = denseLayerSizes)

	# Load a previously trained network if instructed
	if loadModel:
		model = model.loadModel(inFile=configFile)
		x = model.inputs

	# Define the cost function
	cost = model.finetuneCost(y) + L1Param*model.l1Regularization() + L2Param*model.l2Regularization()


	# Compile Theano functions to compute the test and validation error of the model
	testModel = theano.function(
		inputs=[index],
		outputs=model.errors(y),
		givens={x: test_set_x[index*batchSize:(index + 1)*batchSize], y: test_set_y[index*batchSize:(index + 1)*batchSize]}
		)

	validateModel = theano.function(
		inputs=[index],
		outputs=model.errors(y),
		givens={x: valid_set_x[index*batchSize:(index + 1)*batchSize], y: valid_set_y[index*batchSize:(index + 1)*batchSize]}
		)

	epoch = 0

	# Specify how to update the parameters of the model as a list of (variable, update expression) pairs
	updates = []
	for param in model.params:
		# Create a paramUpdate shared variable for each model parameter
		# which will track that parameters update step across iterations.
		# It is initialized with 0.
		paramUpdate = T._shared(param.get_value()*0., broadcastable=param.broadcastable)
	
		# Each parameter is updated by taking a step in the direction
		# of the gradient, we also incorporate a portion of the previous
		# step based upon the momentum parameter.
		updates.append((param, param - (learningRate/(2**(epoch*learningRateDecay)))*paramUpdate))
		
		updates.append((paramUpdate, momentum*paramUpdate + (1. - momentum)*T.grad(cost, param)))

	# Compile a Theano function `trainModel` that returns the cost and
	# updates the parameters of the model based on the rules defined
	# in the updates variable
	trainModel = theano.function(inputs=[index],
								 outputs=cost,
								 updates=updates,
								 givens={x: train_set_x[index*batchSize: (index + 1)*batchSize], y: train_set_y[index*batchSize: (index + 1)*batchSize]})

	print('Training... \n')

	# Early-stopping parameters
	# Look at a minimum of this many examples
	patience = 768

	# Wait this much longer when a new best is found
	patience_increase = 2

	# A relative improvement of this much is considered significant
	improvement_threshold = 0.995

	# Go through this many mini-batches before checking the network on
	# the validation set, currently validates after every epoch
	validationFrequency = nTrainBatches

	
	plotLosses = []
	validEpoch = 0
	bestValidation = np.inf
	testEpoch = 0
	testScore = np.inf
	bestTestScore = np.inf
	start_time = timeit.default_timer()

	haltCondition = False

	while (epoch < maxTrainEpochs) and (not haltCondition):
		epoch += 1
		for minibatchIndex in range(nTrainBatches):

			minibatch_avg_cost = trainModel(minibatchIndex)

			print("Epoch %i, mini-batch %i: %G\n"%(epoch, minibatchIndex, minibatch_avg_cost))

			iter = (epoch - 1)*nTrainBatches + minibatchIndex

			if (iter+1)%validationFrequency == 0:
				validationLosses = [validateModel(i) for i in range(nValidBatches)]
				validationLoss = np.mean(validationLosses)
				plotLosses.append(validationLoss)

				print('Epoch %i, mini-batch %i/%i, validation error %f %%' %(epoch, minibatchIndex+1, nTrainBatches, validationLoss*100.))

				# if we got the best validation score until now
				if validationLoss < bestValidation:
					#improve patience if loss improvement is good enough
					if (validationLoss < bestValidation*improvement_threshold):
						patience = max(patience, iter * patience_increase)

					bestValidation = validationLoss
					validEpoch = epoch

					# test it on the test set
					test_losses = [testModel(i) for i in range(nTestBatches)]
					testScore = np.mean(test_losses)

					if testScore < bestTestScore:
						testEpoch = epoch
						bestTestScore = testScore

						if configFile:
							model.saveModel(outFile=configFile)

					print('\tEpoch %i, mini-batch %i/%i, test error of best model %f %%' %(epoch, minibatchIndex+1, nTrainBatches, testScore*100.))
					
			if patience <= iter:
				haltCondition = True
				break

			if math.isnan(minibatch_avg_cost) or minibatch_avg_cost==np.inf:
				haltCondition = True
				print('Training has diverged or encountered a similar issue. Exiting...')
				break

	end_time = timeit.default_timer()
	print('Optimization complete. Best validation score of %f %% at epoch %i with test performance %f %%' %(bestValidation*100., validEpoch, testScore*100.))
	print('The code for file ' + os.path.split(__file__)[1] + ' ran for %.2fm' %((end_time-start_time)/60.))

	if configFile:
		model = model.loadModel(inFile=configFile)

	if showFigs:
		plt.plot(range(len(plotLosses)), plotLosses, 'bx-')
		plt.savefig("../Visualizations/trainingErrorVis.pdf", bbox_inches='tight')
		plt.savefig("../Visualizations/trainingErrorVis.png", bbox_inches='tight')

		#model.displayFilters(numConvLayers, len(denseLayerSizes), numFilters)

		model.evaluate(10, test_set_x, test_set_y, index, batchSize)

		plt.show()

	return [bestTestScore, model]


if __name__ == '__main__':
	output = trainCAE(learningRate = .01, 
					 learningRateDecay = 1.,
					 L1Param = 0.,
					 L2Param = 0.48,
					 momentum = 0.9,
					 batchSize = 10,
					 filterShape = [3, 3],
					 numFilters = 60,
					 maxTrainEpochs = 5,
					 numConvLayers = 6,
					 denseLayerSizes = [4*4*60, 4*4*60, 4*4*60],
					 configFile = '../ModelConfigs/caeConfig.pkl',
					 loadModel = False,
					 showFigs = True)