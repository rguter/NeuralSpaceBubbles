"""!
@brief Contains layer objects which may be used to construct neural networks

@details Basic layers needed to construct multilayer perceptrons,
convolutional neural networks, convolutional auto-encoders, and more
are implemented.

@author Colin M. Van Oort
"""

from theano.tensor.nnet import conv2d
from theano.tensor.signal import downsample
import numpy as np
import theano
import theano.tensor as T


class BatchNormalizationLayer(object):
	"""!
	@brief This layer implements batch normlazation which is used to
	reduce internal covariate shift.

	@details This layer implements batch normalization in a modular fashion
	so that it may be used in various neural architectures with minimal
	difficulty or alteration.

	Batch normalization is a technique which is used to keep the outputs
	of each layer closer to the 0 mean, unit variance which is usually
	passed into machine learning models.
	This is achieved by renormalizing mini-batches directly before each
	application of a non-linearity.

	Batch normalization helps to stabilize the training of deep neural
	networks.
	"""
	def __init__(self, rng, inputs, inputShape):
		"""!
		@param rng (numpy.random.RandomState) A random number generator used to initialize weights

		@param inputs (theano.tensor.dtensor4) Symbolic image tensor of shape inputShape

		@param inputShape (tuple or list of 4 int objects) The anticipated input shape (batch size, num feature maps, image height, image width)
		"""
		self.Gamma = T._shared(np.asarray(np.ones((1, inputShape[1], 1, 1)),
							   dtype=theano.config.floatX),
							   broadcastable=[True, False, True, True],
							   borrow=True)

		self.Beta = T._shared(np.asarray(np.zeros((1, inputShape[1], 1, 1)),
							  dtype=theano.config.floatX),
							  broadcastable=[True, False, True, True],
							  borrow=True)

		normalized = (inputs-T.mean(inputs, axis=(0,2,3), keepdims=True))/(T.std(inputs, axis=(0,2,3), keepdims=True)+np.finfo(np.float32).eps)
		self.output = self.Gamma*normalized + self.Beta

		self.outputShape = inputShape
		self.params = [self.Gamma, self.Beta]


class CAEConvLayer(object):
	"""!
	This layer object implements the main building block of the encoder
	for our Convolutional Auto-Encoder model. It sequentially performs 
	a 2D convolution, max pooling, batch normalizaiton, and rectified 
	linear function.
	"""

	def __init__(self, rng, inputs, filterShape, inputShape, poolSize=(2, 2), batchNormalization=True):
		"""!
		@param rng (numpy.random.RandomState) A random number generator used to initialize weights

		@param inputs (theano.tensor.dtensor4) Symbolic image tensor of shape inputShape

		@param filterShape (tuple or list of 4 int objects) The desired filter shape (number of feature maps at layer i, num feature maps at layer i-1, filter height, filter width)

		@param inputShape (tuple or list of 4 int objects) The anticipated input shape (batch size, num feature maps, image height, image width)

		@param poolSize (tuple or list of 2 int objects) The down sampling (pooling) factor (# of rows, # of cols)

		@param batchNormalization (bool) Switch to turn batch normalization on or off
		"""

		# The depth of the filter should equal the number of feature maps in the image
		assert inputShape[1] == filterShape[1]

		self.inputs = inputs

		# There are num feature maps * filter height * filter width 
		# inputs to each hidden unit
		fan_in = np.prod(filterShape[1:])

		# Initialize the weight matrix W with random weights using the
		# Kaiming initialization scheme
		W_bound = np.sqrt(2./fan_in)
		self.W = T._shared(np.asarray(rng.normal(loc=0, scale=W_bound, size=filterShape), dtype=theano.config.floatX), borrow=True)

		# When using batch normalization initialize scale and shift parameters
		if batchNormalization:
			self.Gamma = T._shared(np.asarray(np.ones((1, filterShape[0], 1, 1)),
								   dtype=theano.config.floatX),
								   broadcastable=[True, False, True, True],
								   borrow=True)

			self.Beta = T._shared(np.asarray(np.zeros((1, filterShape[0], 1, 1)),
								  dtype=theano.config.floatX),
								  broadcastable=[True, False, True, True],
								  borrow=True)

		# Convolve the input feature maps with the filters
		conv_out = conv2d(input=self.inputs,
						  filters=self.W,
						  filter_shape=filterShape,
						  input_shape=inputShape,
						  border_mode='half')

		# Downsample each feature map individually using max pooling
		pooled_out = downsample.max_pool_2d(conv_out, poolSize, ignore_border=False)

		# The non-linearity is applied here, if batch normalization is
		# being used then batch normalization occurs first
		if batchNormalization:
			normalized = (pooled_out-T.mean(pooled_out, axis=(0,2,3), keepdims=True))/(T.std(pooled_out, axis=(0,2,3), keepdims=True)+np.finfo(np.float32).eps)
			bn = self.Gamma*normalized + self.Beta
			self.output = T.nnet.relu(bn, alpha=0.01)

		else:
			self.output = T.nnet.relu(pooled_out, alpha=0.01)

		# Output shape is used to chain layers
		self.outputShape = (inputShape[0], filterShape[0], inputShape[2]//2, inputShape[3]//2)

		# Store parameters of this layer
		self.params = [self.W, self.Gamma, self.Beta]


class CAEDeconvLayer(object):
	"""!
	This layer object implements the main building block of the decoder
	for our Convolutional Auto-Encoder model. It sequentially performs
	batch normalization, a recitfied linear function, simple upsampling
	(de-pooling), and 2-D convolution.
	"""

	def __init__(self, rng, inputs, filterShape, inputShape, poolSize=(2, 2), batchNormalization=True):
		"""!
		@param rng (numpy.random.RandomState) A random number generator used to initialize weights

		@param inputs (theano.tensor.dtensor4) Symbolic image tensor of shape inputShape

		@param filterShape (tuple or list of 4 int objects) The desired filter shape (number of feature maps at layer i, num feature maps at layer i-1, filter height, filter width)

		@param inputShape (tuple or list of 4 int objects) The anticipated input shape (batch size, num feature maps, image height, image width)

		@param poolSize (tuple or list of 2 int objects) The up sampling (de-pooling) factor (# of rows, # of cols)

		@param batchNormalization (bool) Switch to turn batch normalization on or off
		"""

		assert inputShape[1] == filterShape[1]

		self.inputs = inputs

		fan_in = np.prod(filterShape[1:])

		W_bound = np.sqrt(2./fan_in)
		self.W = T._shared(np.asarray(rng.normal(loc=0, scale=W_bound, size=filterShape), dtype=theano.config.floatX), borrow=True)

		if batchNormalization:
			self.Gamma = T._shared(np.asarray(np.ones((1, filterShape[1], 1, 1)),
								   dtype=theano.config.floatX),
								   broadcastable=[True, False, True, True],
								   borrow=True)

			self.Beta = T._shared(np.asarray(np.zeros((1, filterShape[1], 1, 1)),
								  dtype=theano.config.floatX),
								  broadcastable=[True, False, True, True],
								  borrow=True)

			normalized = (inputs-T.mean(inputs, axis=(0,2,3), keepdims=True))/(T.std(inputs, axis=(0,2,3), keepdims=True)+np.finfo(np.float32).eps)
			bn = self.Gamma*normalized + self.Beta
			nonLin = T.nnet.relu(bn, alpha=0.01)

		else:
			nonLin = T.nnet.relu(inputs, alpha=0.01)

		# Up sampling/de-pooling
		depool = nonLin.repeat(poolSize[0], axis=2).repeat(poolSize[1], axis=3)
		depooledShape = (inputShape[0], inputShape[1], inputShape[2]*2, inputShape[3]*2)

		# Convolution is a symmetric operation except for at the edges
		# of the image, thus we use a convolution as an inverse operation
		# to the convolution performed in the CAEConvLayer.
		self.output = conv2d(input=depool, filters=self.W, filter_shape=filterShape, input_shape=depooledShape, border_mode='half')

		self.outputShape = (inputShape[0], filterShape[0], inputShape[2]*2, inputShape[3]*2)

		self.params = [self.W, self.Gamma, self.Beta]

	def mse(self, target):
		"""!
		@param target (theano.tensor.dtensor4) Tensor containing the target outputs

		@return (float) Mean squared error between the output of the layer and the target
		"""
		return T.mean(T.sqr(target-self.output))

	def mae(self, target):
		"""!
		@param target (theano.tensor.dtensor4) Tensor containing the target outputs

		@return (float) Mean absolute error between the output of the layer and the target
		"""
		return T.mean(T.abs_(target-self.output))

	def modifiedMAE(self, target):
		"""!
		@details Calculates the sum of the absolute errors in the last
		three dimensions and then calculates the average of those sums.

		@param target (theano.tensor.dtensor4) Tensor containing the target outputs

		@return (float) Modified mean absolute error between the output of the layer and the target
		"""
		return T.mean(T.sum(T.abs_(target-self.output), axis=[1,2,3]))

	def relativeError(self, target):
		"""!
		@param target (theano.tensor.dtensor4) Tensor containing the target outputs

		@return (float) Mean relative error between the output of the layer and the target
		"""
		return T.mean(T.abs_((target-self.output)/(target+np.finfo(np.float32).eps)))


class ConvolutionLayer(object):
	"""!
	This layer object performs a single convolution operation.

	*** Currently not implemented ***
	"""
	def __init__(self, rng, inputs, filterShape, inputShape, poolSize=(2, 2), batchNormalization=True):
		"""!
		@param rng (numpy.random.RandomState) A random number generator used to initialize weights

		@param inputs (theano.tensor.dtensor4) Symbolic image tensor of shape inputShape

		@param filterShape (tuple or list of 4 int objects) Desired filter shape (number of feature maps at layer i, num feature maps at layer i-1, filter height, filter width)

		@param inputShape (tuple or list of 4 int objects) Anticipated input shape (batch size, num feature maps, image height, image width)

		@param poolSize (tuple or list of 2 int objects) Up sampling (de-pooling) factor (# of rows, # of cols)

		@param batchNormalization (bool) Switch to turn batch normalization on or off
		"""

		pass


class DenseLayer(object):
	"""!
	This layer object implements the internal layers of our Convolutional
	Auto-Encoder model which manipulate the encoded representation.
	"""
	def __init__(self, rng, inputs, n_in, n_out, activation=T.nnet.relu, batchNormalization=True):
		"""!
		@param rng (numpy.random.RandomState) A random number generator used to initialize weights

		@param inputs (theano.tensor.dtensor4) Symbolic image tensor of shape inputShape

		@param n_in (int) Anticipated input dimension (assumed to be 1-D)

		@param n_out (int) Desired number of outputs, also the number of hidden units

		@param activation (theano.Op or function) Non-linearity to be applied in the hidden layer

		@param batchNormalization (bool) Switch to turn batch normalization on or off
		"""
		self.inputs = inputs

		W_values = np.asarray(rng.normal(loc=0, scale=np.sqrt(2./n_in), size=(n_in, n_out)), dtype=theano.config.floatX)
		self.W = T._shared(value=W_values, name='W', borrow=True)

		linearActivation = T.dot(self.inputs, self.W)

		if batchNormalization:
			self.Gamma = T._shared(np.asarray([[1.]], dtype=theano.config.floatX), broadcastable=[True, True], borrow=True)
			self.Beta = T._shared(np.asarray([[0.]], dtype=theano.config.floatX), broadcastable=[True, True], borrow=True)

			normalized = (linearActivation-T.mean(linearActivation, keepdims=True))/(T.std(linearActivation, keepdims=True)+np.finfo(np.float32).eps)
			bn = self.Gamma*normalized + self.Beta
			self.output = activation(bn, alpha=0.01)

		else:
			self.output = activation(linearActivation)


		self.size = n_out

		self.params = [self.W, self.Gamma, self.Beta]


class FlattenLayer(object):
	"""!
	This layer object performs a reversible flattening operation which
	is used as an interface between the encoder and the internal representation
	of our Convolutional Auto-Encoder model.
	"""
	def __init__(self, inputs, inputShape):
		"""!
		@param inputs (theano.tensor.dtensor4) Symbolic image tensor of shape inputShape

		@param inputShape (tuple or list of 4 int objects) Anticipated input shape (batch size, num feature maps, image height, image width)
		"""
		self.inputs = inputs
		self.output = self.inputs.flatten(2)
		self.size = inputShape[1]*inputShape[2]*inputShape[3]


class MaxPoolLayer(object):
	"""!
	This layer object performs simple max pooling down sampling operation.

	*** Currently not implemented ***
	"""
	def __init__():
		"""!

		"""

		pass


class ReLuLayer(object):
	"""!
	This layer object performs a leaky rectified linear function.

	*** Currently not implemented ***
	"""
	def __init__():
		"""!

		"""

		pass


class ReshapeLayer(object):
	"""!
	This layer object performs a reshaping operation which is used to
	reverse the operation performed by the FlattenLayer. It is used as
	an interface between the internal representation and the decoder of
	our Convolutional Auto-Encoder model.
	"""
	def __init__(self, inputs, reshapeDims):
		"""!
		@param inputs (theano.tensor.dtensor4) Symbolic image tensor of shape inputShape

		@param reshapDims (tuple or list of 4 int objects) The desired output shape (batch size, num feature maps, image height, image width) **Should be identical to the dimension of the output of the encoder**
		"""
		self.inputs = inputs
		self.output = self.inputs.reshape(reshapeDims)
		self.outputShape = reshapeDims


class SimpleUpsampleLayer(object):
	"""!
	This layer object performs a simple replication upsampling operation
	which rescales the input by an integer factor in each of its dimensions.

	*** Currently not implemented ***
	"""
	def __init__():
		"""!

		"""
		
		pass