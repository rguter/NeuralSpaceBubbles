from theano.tensor.nnet import conv2d
from theano.tensor.signal import downsample
import math
import matplotlib.pyplot as plt
import numpy
import os
import pickle
import sys
import theano
import theano.tensor as T
import timeit


class CAEConvLayer(object):
	"""
		The first layers of a convolutional auto-encoder.
		Performs a 2D convolution, max pooling, and tanh nonlinearity on the input image
	"""

	def __init__(self, rng, inputs, filterShape, inputShape, poolSize=(2, 2)):
		"""
			rng: numpy.random.RandomState
				 a random number generator used to initialize weights

			inputs: theano.tensor.dtensor4
					inputs: symbolic image tensor of shape inputShape

			filterShape: tuple or list of length 4
						 (number of feature maps at layer i, num feature maps at layer i-1, filter height, filter width)

			inputShape: tuple or list of length 4
						(batch size, num feature maps, image height, image width)

			poolSize: tuple or list of length 2
					  the downsampling (pooling) factor (#rows, #cols)
		"""

		# The depth of the filter should equal the number of feature maps in the image
		assert inputShape[1] == filterShape[1]

		self.inputs = inputs

		# there are num feature maps * filter height * filter width inputs to each hidden unit
		fan_in = numpy.prod(filterShape[1:])

		# each unit in the lower layer receives a gradient from: num output feature maps * filter height * filter width/pooling size
		fan_out = (filterShape[0]*numpy.prod(filterShape[2:])/numpy.prod(poolSize))

		# initialize the weight matrix W with random weights
		W_bound = numpy.sqrt(6./(fan_in + fan_out))
		self.W = T._shared(numpy.asarray(rng.uniform(low=-W_bound, high=W_bound, size=filterShape), dtype=theano.config.floatX), borrow=True)

		# the bias is a 1D tensor with one bias value per output feature map
		b_values = numpy.zeros((filterShape[0]), dtype=theano.config.floatX)
		self.b = T._shared(value=b_values, borrow=True)

		# convolve the input feature maps with the filters, default border_mode is valid which provides an output of inputDim - filterDim + 1
		conv_out = conv2d(input=self.inputs, filters=self.W, filter_shape=filterShape, input_shape=inputShape, border_mode='half')

		# downsample each feature map individually using max pooling
		pooled_out = downsample.max_pool_2d(conv_out, poolSize, ignore_border=False)

		# Add the bias and then apply the tanh nonlinearity, dimshuffle is used to broadcast the bias across the entire image and all of its feature maps
		self.output = T.tanh(pooled_out + self.b.dimshuffle('x', 0, 'x', 'x'))

		# output shape is used to chain layers
		self.outputShape = (inputShape[0], filterShape[0], inputShape[2]//2, inputShape[3]//2)

		# store parameters of this layer
		self.params = [self.W, self.b]


class FlattenLayer(object):
	"""
		This layer object takes a 4D tensor of input images and flattens them into a 2D structure
	"""
	def __init__(self, inputs, inputShape):
		self.inputs = inputs
		self.output = self.inputs.flatten(2)
		self.size = inputShape[1]*inputShape[2]*inputShape[3]


class DenseLayer(object):
	def __init__(self, rng, input, n_in, n_out, W=None, b=None, activation=T.tanh):
		"""
			rng: numpy.random.RandomState
				 a random number generator used to initialize weights

			input: theano.tensor.dmatrix
				   a symbolic tensor of shape (n_examples, n_in)

			n_in: int
				  dimensionality of input

			n_out: int
				   n_out: number of hidden units

			activation: theano.Op or function
						Non linearity to be applied in the hidden layer
		"""
		self.input = input
		if W is None:
			W_values = numpy.asarray(rng.uniform(low=-numpy.sqrt(6./(n_in + n_out)), high=numpy.sqrt(6./(n_in + n_out)), size=(n_in, n_out)), dtype=theano.config.floatX)

			if activation == theano.tensor.nnet.sigmoid:
				W_values *= 4

			W = T._shared(value=W_values, name='W', borrow=True)

		if b is None:
			b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
			b = T._shared(value=b_values, name='b', borrow=True)

		self.W = W
		self.b = b

		lin_output = T.dot(input, self.W) + self.b
		self.output = (lin_output if activation is None else activation(lin_output))

		self.params = [self.W, self.b]


class ReshapeLayer(object):
	"""
		This layer object takes a mini-batch of vectors and reshapes it into a 4D image tensor
	"""
	def __init__(self, inputs, reshapeDims):
		self.inputs = inputs
		self.output = self.inputs.reshape(reshapeDims)
		self.outputShape = reshapeDims


class CAEDeconvLayer(object):
	"""
		The final layers a convolutional auto-encoder.
		Performs tanh nonlinearity, depooling, and 2D convolution on the input image
	"""

	def __init__(self, rng, inputs, filterShape, inputShape, poolSize=(2, 2)):
		"""
			rng: numpy.random.RandomState
				 a random number generator used to initialize weights

			inputs: theano.tensor.dtensor4
					 inputs: symbolic image tensor of shape inputShape

			filterShape: tuple or list of length 4
						 (number of feature maps at layer i, num feature maps at layer i-1, filter height, filter width)

			inputShape: tuple or list of length 4
						(batch size, num feature maps, image height, image width)

			poolSize: tuple or list of length 2
					  the upsampling (depooling) factor (#rows, #cols)
		"""

		assert inputShape[1] == filterShape[1]

		self.inputs = inputs

		fan_in = numpy.prod(filterShape[1:])

		fan_out = (filterShape[0] * numpy.prod(filterShape[2:])/numpy.prod(poolSize))

		W_bound = numpy.sqrt(6./(fan_in + fan_out))
		self.W = T._shared(numpy.asarray(rng.uniform(low=-W_bound, high=W_bound, size=filterShape), dtype=theano.config.floatX), borrow=True)

		# The number of biases is based upon the number of feature maps at the previous layer since the nonlinearity is performed before the convolution in this layer
		b_values = numpy.zeros((filterShape[1]), dtype=theano.config.floatX)
		self.b = T._shared(value=b_values, borrow=True)

		nonLin = T.tanh(inputs + self.b.dimshuffle('x', 0, 'x', 'x'))

		# upsample the image
		depool = nonLin.repeat(poolSize[0], axis=2).repeat(poolSize[1], axis=3)
		depooledShape = (inputShape[0], inputShape[1], inputShape[2]*2, inputShape[3]*2)

		# Convolution is a symmetric operation except for at the edges of the image, thus we use a convolution as an inverse operation to the convolution performed in the CAEConvLayer. The border_mode is changed to full rather than vaild, which provides an output of inputDim + filterDim - 1, which returns us to the original input dimensions when paired with a CAEConvLayer.
		self.output = conv2d(input=depool, filters=self.W, filter_shape=filterShape, input_shape=depooledShape, border_mode='half')

		self.outputShape = (inputShape[0], filterShape[0], inputShape[2]*2, inputShape[3]*2)

		self.params = [self.W, self.b]

	def mse(self, target):
		return T.mean(T.sqr(target-self.output))

	def sse(self, target):
		return T.sum(T.sqr(target-self.output))

	def mae(self, target):
		return T.mean(T.abs_(target-self.output))

	def sae(self, target):
		return T.sum(T.abs_(target-self.output))

	def relativeError(self, target):
		return T.mean(T.abs_((target-self.output)/target))
		


class SimpleCAE(object):
	"""
		A simple convolutional auto-encoder prototype designed to train on the ML_Bubbles project data and identify bubbles in cosmic gas
	"""

	def __init__(self, rng, inputs, filterShape, inputShape, poolSize=(2,2)):
		"""
			The currrent model architecture consists of three convolutional layers with max pooling, a single dense layer, and three deconvolutional layers with upsampling

			*** Note *** Use a filter with an odd number of rows and columns to cause convolutions to have identical dimensions for thier inputs and outputs (makes layering much simpler)
			
			+ Changing error functions can cause some crazy behavior, MSE and MAE both seem to work well but the SSE and SAE seem to cause the validation and test error rates to explode

			+ The validation and test errors are identical for some reason, investigation required
		"""

		assert inputShape[1] == filterShape[1]
		self.rng = rng

		self.inputs = inputs
		self.layers = []

		convLayer0 = CAEConvLayer(self.rng, self.inputs, filterShape, inputShape, poolSize)
		self.layers.append(convLayer0)

		# filterShape is altered internally to allow the number of filters to be altered
		filterShape2 = (filterShape[0], filterShape[0], filterShape[2], filterShape[3])

		convLayer1 = CAEConvLayer(self.rng, convLayer0.output, filterShape2, convLayer0.outputShape, poolSize)
		self.layers.append(convLayer1)

		convLayer2 = CAEConvLayer(self.rng, convLayer1.output, filterShape2, convLayer1.outputShape, poolSize)
		self.layers.append(convLayer2)

		flattenLayer = FlattenLayer(convLayer2.output, convLayer2.outputShape)

		denseLayer = DenseLayer(self.rng, flattenLayer.output, flattenLayer.size, flattenLayer.size)
		self.layers.append(denseLayer)

		reshapeLayer = ReshapeLayer(denseLayer.output, convLayer2.outputShape)

		deconvLayer0 = CAEDeconvLayer(self.rng, reshapeLayer.output, filterShape2, reshapeLayer.outputShape, poolSize)
		self.layers.append(deconvLayer0)

		deconvLayer1 = CAEDeconvLayer(self.rng, deconvLayer0.output, filterShape2, deconvLayer0.outputShape, poolSize)
		self.layers.append(deconvLayer1)

		# filterShape is altered again in order to force the output to have the same dimensions as the input
		filterShape3 = (filterShape[1], filterShape[0], filterShape[2], filterShape[3])

		deconvLayer2 = CAEDeconvLayer(self.rng, deconvLayer1.output, filterShape3, deconvLayer1.outputShape, poolSize)
		self.layers.append(deconvLayer2)

		self.params = []
		for layer in self.layers:
			self.params.extend(layer.params)

		# The cost of the model is the mean of the absolute errors of the output of the final layer w.r.t. the target
		self.finetuneCost = self.layers[-1].mae

		# The error function used in validation and testing is the mean of the elementwise relative error of the final layer w.r.t the target
		self.errors = self.layers[-1].relativeError

		self.output = self.layers[-1].output

	def l1Regularization(self):
		output = 0
		for layer in self.layers:
			output += T.sum(T.abs_(layer.W))

		return output

	def l2SquaredRegularization(self):
		output = 0
		for layer in self.layers:
			output += T.sum(T.pow(layer.W, 2))

		return output

	def displayFilters(self, numConvLayers, numDenseLayers, numFilters):
		"""
			Creates a visualization of the filters of the network, saves .pdf and .png versions
		"""
		filters = []
		layerNum = 0

		# Grab the filters from the convolution and decovolution layers
		for i in range(numConvLayers):
			filters.append(self.layers[layerNum].W)
			layerNum += 1

		for j in range(numDenseLayers):
			layerNum += 1

		for k in range(numConvLayers):
			filters.append(self.layers[layerNum].W)

		# Unstack the filters into 2D images
		unstackedFilters = []
		for element in filters:
			filterVal = element.get_value()
			filterShape = filterVal.shape

			unstackingFilters = []
			
			for i in range(filterShape[0]):
				for j in range(filterShape[1]):
					unstackingFilters.append(filterVal[i,j,:,:])

			unstackedFilters.append(unstackingFilters)

		fig,axes = plt.subplots(6,numFilters)
		# Need to increase the space between subplots

		for i in range(6):
			indexList = self.rng.randint(numFilters, size=numFilters)

			for j in range(numFilters):
				if i:
					axes[i, j].axis('off')
					axes[i, j].imshow(unstackedFilters[i][indexList[j]], cmap=plt.get_cmap('Greys'), interpolation="nearest")

				else:
					axes[i, j].axis('off')
					axes[i, j].imshow(unstackedFilters[i][j], cmap=plt.get_cmap('Greys'), interpolation="nearest")

		plt.savefig("filterVis.png")
		plt.savefig("filterVis.pdf")

	def getWeights(self):
		weights = []
		for layer in self.layers:
			weights.append([layer.W.get_value(), layer.b.get_value()])

		return weights

	def setWeights(self, weights):
		for layer, weight in zip(self.layers, weights):
				layer.W.set_value(weight[0])
				layer.b.set_value(weight[1])

	def saveWeights(self, fileName='weightConfig.pkl'):
		"""
			Saves the current weight configuration of the network to a file for later use
		"""
		weights = self.getWeights()

		with open(fileName, 'wb') as pickleFile:
			pickle.dump(weights, pickleFile)

	def loadWeights(self, fileName='weightConfig.pkl'):
		"""
			Initialzes the network with weights obtained from a configuration file
		"""
		with open(fileName, 'rb') as pickleFile:
			weights = pickle.load(pickleFile)

		if len(weights) != len(self.layers):
			raise ValueError("The config file %s has weights and biases for %d layers, but the network requires %d pairs of weights and biases!\n" %(fileName, len(weights), len(self.layers)))

		else:
			self.setWeights(weights)
			

	def evaluate(self, numOutputs, inputs, targets, index, x, batchSize):
		"""
			Creates and saves a visualization of numOutputs image, target, network output triples
		"""
		evaluateModel = theano.function(inputs=[index], outputs=self.output, givens={x:inputs[index*batchSize:(index + 1)*batchSize]})

		indexList = self.rng.choice(numpy.arange(inputs.get_value().shape[0]//batchSize), size=math.ceil(numOutputs/batchSize), replace=False)
		inputArray = None
		targetArray = None
		outputArray = None
		for i in range(math.ceil(numOutputs/batchSize)):
			modelOut = evaluateModel(indexList[i])

			if i:
				inputArray = numpy.concatenate((inputArray, inputs.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, : , :, :]))
				targetArray = numpy.concatenate((targetArray, targets.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, :, :, :]))
				outputArray = numpy.concatenate((outputArray, modelOut))

			else:
				inputArray = inputs.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, :, :, :]
				targetArray = targets.get_value()[indexList[i]*batchSize:(indexList[i] + 1)*batchSize, :, :, :]
				outputArray = modelOut

		imageArrays = [inputArray, targetArray, outputArray]
			
		fig, axes = plt.subplots(numOutputs, 3)
		axCount = 0

		for i in range(numOutputs):
			for array in imageArrays:
				axes[i, axCount].axis('off')
				axes[i, axCount].imshow(array[i, 0, :, :], cmap=plt.get_cmap('Greys'), interpolation='nearest')
				axCount += 1
			axCount = 0

		plt.savefig("comparisonVis.pdf")
		plt.savefig("comparisonVis.png")


def load_data(dataset):
	"""
		dataset: string
				 the path to the dataset
	"""

	with open(dataset, 'rb') as pickleFile:
		# train_set, valid_set, test_set are tuples of numpy.ndarray which contain the input data and their corresponding target data
		train_set, valid_set, test_set = pickle.load(pickleFile)

	def shared_dataset(data_xy, borrow=True):
		""" 
			Function that loads the dataset into shared variables, which allow for the GPU to be efficiently utilized
		"""
		data_x, data_y = data_xy

		# Ensure that the input has the correct number of dimensions, even if it doesn't have multiple channels
		if len(data_x.shape) != 4:
			data_x = numpy.expand_dims(data_x, axis=1)
			data_y = numpy.expand_dims(data_y, axis=1)

		shared_x = T._shared(data_x.astype(numpy.float32), borrow=borrow)
		shared_y = T._shared(data_y.astype(numpy.float32), borrow=borrow)

		return shared_x, shared_y

	train_set_x, train_set_y = shared_dataset(train_set)
	valid_set_x, valid_set_y = shared_dataset(valid_set)
	test_set_x, test_set_y = shared_dataset(test_set)
	
	dataObject = [(train_set_x, train_set_y), (valid_set_x, valid_set_y), (test_set_x, test_set_y)]
	
	return dataObject

def testCAE(learningRate=0.01, learningRateDecay=.2, L1Param=0.001, L2Param=0.001, momentum=0.9, batchSize=10, numFilters=10, maxTrainEpochs=100, dataset='bubbleDataset.pkl', weightFile=None, loadWeights=False, showFigs=True):
	assert momentum < 1 and momentum >= 0

	datasets = load_data(dataset)

	train_set_x, train_set_y = datasets[0]
	valid_set_x, valid_set_y = datasets[1]
	test_set_x, test_set_y = datasets[2]

	# compute number of minibatches for training, validation and testing
	nTrainBatches = train_set_x.get_value(borrow=True).shape[0] // batchSize
	n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batchSize
	n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batchSize

	print('Building model...\n')

	# allocate symbolic variables for the data
	# index to a [mini]batch
	index = T.lscalar()  
	# the data is presented as a 4D tensor of images
	x = T.ftensor4('x')
	# the flag is presented as a 4D tensor of images
	y = T.ftensor4('y')

	# construct the CAE
	rng = numpy.random.RandomState()

	model = SimpleCAE(rng=rng, inputs=x, filterShape=(numFilters,1,5,5), inputShape=(batchSize,1,256,256))

	if loadWeights:
		model.loadWeights(fileName=weightFile)

	cost = model.finetuneCost(y)/batchSize + L1Param*model.l1Regularization() + L2Param*model.l2SquaredRegularization()

	# compile Theano functions to compute the test and validation error of the model
	testModel = theano.function(
		inputs=[index],
		outputs=model.errors(y),
		givens={x: test_set_x[index*batchSize:(index + 1)*batchSize], y: test_set_y[index*batchSize:(index + 1)*batchSize]}
		)

	validateModel = theano.function(
		inputs=[index],
		outputs=model.errors(y),
		givens={x: valid_set_x[index*batchSize:(index + 1)*batchSize], y: valid_set_y[index*batchSize:(index + 1)*batchSize]}
		)

	epoch = 0

	# specify how to update the parameters of the model as a list of (variable, update expression) pairs
	updates = []
	for param in model.params:
		# For each parameter, we'll create a paramUpdate shared variable. This variable will keep track of the parameter's update step across iterations. We initialize it to 0
		paramUpdate = T._shared(param.get_value()*0., broadcastable=param.broadcastable)
		# Each parameter is updated by taking a step in the direction of the gradient. However, we also "mix in" the previous step according to the given momentum value. Note that when updating paramUpdate, we are using its old value and also the new gradient step.
		updates.append((param, param - (learningRate/(1.0 + epoch*learningRateDecay))*paramUpdate))
		
		updates.append((paramUpdate, momentum*paramUpdate + (1. - momentum)*T.grad(cost, param)))

	# compile a Theano function `trainModel` that returns the cost and updates the parameters of the model based on the rules defined in `updates`
	trainModel = theano.function(
		inputs=[index],
		outputs=cost,
		updates=updates,
		givens={x: train_set_x[index*batchSize: (index + 1)*batchSize], y: train_set_y[index*batchSize: (index + 1)*batchSize]}
		)

	print('Training... \n')

	# early-stopping parameters
	# look at a minimum of this many examples
	patience = 10000
	# wait this much longer when a new best is found
	patience_increase = 2
	# a relative improvement of this much is considered significant
	improvement_threshold = 0.995
	# go through this many minibatches before checking the network on the validation set; in this case we check every epoch
	validationFrequency = min(nTrainBatches, patience // 2)

	
	plotLosses = []
	validEpoch = 0
	bestValidation = numpy.inf
	testEpoch = 0
	testScore = 0.
	bestTestScore = numpy.inf
	start_time = timeit.default_timer()

	haltCondition = False

	while (epoch < maxTrainEpochs) and (not haltCondition):
		epoch += 1
		for minibatchIndex in range(nTrainBatches):

			minibatch_avg_cost = trainModel(minibatchIndex)

			print("Epoch %i, minibatch %i: %G\n"%(epoch, minibatchIndex, minibatch_avg_cost))

			iter = (epoch - 1)*nTrainBatches + minibatchIndex

			if (iter+1)%validationFrequency == 0:
				validationLosses = [validateModel(i) for i in range(n_valid_batches)]
				validationLoss = numpy.mean(validationLosses)
				plotLosses.append(validationLoss)

				print('Epoch %i, minibatch %i/%i, validation error %f %%' %(epoch, minibatchIndex+1, nTrainBatches, validationLoss*100.))

				# if we got the best validation score until now
				if validationLoss < bestValidation:
					#improve patience if loss improvement is good enough
					if (validationLoss < bestValidation*improvement_threshold):
						patience = max(patience, iter * patience_increase)

					bestValidation = validationLoss
					validEpoch = epoch

					# test it on the test set
					test_losses = [testModel(i) for i in range(n_test_batches)]
					testScore = numpy.mean(test_losses)

					if testScore < bestTestScore:
						testEpoch = epoch
						bestTestScore = testScore

						if weightFile:
							model.saveWeights(fileName=weightFile)

					print(('\tEpoch %i, minibatch %i/%i, test error of best model %f %%') %(epoch, minibatchIndex+1, nTrainBatches, testScore*100.))
					
			if patience <= iter:
			    haltCondition = True
			    break

	end_time = timeit.default_timer()
	print(('Optimization complete. Best validation score of %f %% at epoch %i with test performance %f %%') %(bestValidation*100., validEpoch, testScore*100.))
	print(('The code for file ' + os.path.split(__file__)[1] + ' ran for %.2fm' % ((end_time-start_time)/60.)), file=sys.stderr)

	plt.plot(range(len(plotLosses)), plotLosses, 'bx-')

	plt.savefig("trainingErrorVis.pdf")
	plt.savefig("trainingErrorVis.png")

	if weightFile:
		model.loadWeights(fileName=weightFile)

	model.displayFilters(3, 1, numFilters)

	model.evaluate(3, test_set_x, test_set_y, index, x, batchSize)

	if showFigs:
		plt.show()

	return [bestTestScore, model]


if __name__ == '__main__':
	output = testCAE(learningRate=0.04, learningRateDecay=.2, L1Param=0.001, L2Param=0.001, momentum=0.9, batchSize=10, numFilters=10, maxTrainEpochs=30, weightFile='weightConfig.pkl', loadWeights=False, showFigs=True)
