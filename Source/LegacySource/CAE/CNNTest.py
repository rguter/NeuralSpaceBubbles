from PIL import Image
from theano import tensor as T
from theano.tensor.nnet import conv2d
import numpy
import pickle
import pylab
import theano

def cnnTest():
	"""

	"""

	rng = numpy.random.RandomState()

	input = T.tensor4(name='input')

	w_shp = (20, 1, 5, 5)
	w_bound = numpy.sqrt(1 * 5 * 5)
	W = theano.shared(numpy.asarray(rng.uniform(low=-1.0 / w_bound, high=1.0 / w_bound, size=w_shp), dtype=input.dtype), name ='W')

	b_shp = (20)
	b = theano.shared(numpy.asarray(rng.uniform(low=-.5, high=.5, size=b_shp), dtype=input.dtype), name ='b')

	conv_out = conv2d(input, W, border_mode='half')
	nonlin = T.tanh(conv_out + b.dimshuffle('x', 0, 'x', 'x'))

	w_shp = (1, 20, 5, 5)
	w_bound = numpy.sqrt(5 * 5 * 5)
	W2 = theano.shared(numpy.asarray(rng.uniform(low=-1.0 / w_bound, high=1.0 / w_bound, size=w_shp), dtype=input.dtype), name ='W')

	b_shp = (20)
	b2 = theano.shared(numpy.asarray(rng.uniform(low=-.5, high=.5, size=b_shp), dtype=input.dtype), name ='b2')

	nonlin2 = T.tanh(nonlin + b2.dimshuffle('x', 0, 'x', 'x'))
	output = conv2d(nonlin2, W2, border_mode='half')

	# create theano function to compute filtered images
	f = theano.function([input], output)

	# open random image of dimensions 639x516
	with open("densityAug.pkl", 'rb') as pickleFile:
		data = pickle.load(pickleFile)
		
	img = data[0,:]
	img = numpy.asarray(img, dtype='float32')
	img = img.reshape(256, 256)
	img_ = img.reshape(1, 1, 256, 256)
	filtered_img = f(img_)


	# plot the original image and the outputs of the above operations
	pylab.subplot(1, 2, 1); pylab.axis('off'); pylab.imshow(img)
	pylab.gray();
	pylab.subplot(1, 2, 2); pylab.axis('off'); pylab.imshow(filtered_img[0, 0, :, :])

	pylab.show()


if __name__ == '__main__':
	cnnTest()