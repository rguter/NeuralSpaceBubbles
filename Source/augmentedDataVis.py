"""!
@brief Contains a function which visualizes the augmented data.

@details Implements a simple function which visualizes the augmented data using matplotlib in order to allow for human inspection.

@author Colin M. Van Oort
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle
import random

def augmentedDataVis():
	"""!
	@brief Displays a single pair of randomly selected images.

	@details Randomly selects and displays a single image from the augmented
	NeuralSpaceBubbles dataset along with its corresponding target.
	"""
	
	# Read in the augmented training and target data
	fileNames = ['densityAug.pkl', 'tracerAug.pkl']

	with open(fileNames[0], 'rb') as pickleFile:
		data = pickle.load(pickleFile)

	with open(fileNames[1], 'rb') as pickleFile:
		target = pickle.load(pickleFile)

	# Ensure that the data and target objects have the same dimensions
	assert data.shape == target.shape

	index = random.randint(0, data.shape[0]-1)

	fig, (ax1, ax2) = plt.subplots(1, 2)

	ax1.axis('off')
	ax1.imshow(data[index, :, :], cmap=plt.get_cmap('Greys'), interpolation="nearest")
	ax1.set_title('Altered Data')

	ax2.axis('off')
	ax2.imshow(target[index, :, :], cmap=plt.get_cmap('Greys'), interpolation="nearest")
	ax2.set_title('Altered Target')

	plt.tight_layout()
	
	plt.savefig('../Visualizations/augmentedDataVis.png', bbox_inches='tight')
	plt.savefig('../Visualizations/augmentedDataVis.pdf', bbox_inches='tight')

	plt.show()


if __name__ == '__main__':
	augmentedDataVis()