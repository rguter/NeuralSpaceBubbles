"""!
@brief Analyzes the output of the NeuralSpaceBubbles bubble identifier.

@details Binarizes the input, target, and output of the NeuralSpaceBubbles
bubble identifier and calculates several common binary classification
statistics on the resulting images. Original and binarized image pairs
are plotted together and saved to the project Visualizations directory.
Statistics are aggregated and written out to a log file in the same
directory. More information about the statistics calculated within may
be found on [wikipedia](https://en.wikipedia.org/wiki/Confusion_matrix)

@author Colin M. Van Oort
"""


import matplotlib.pyplot as plt
import numpy as np
import pickle
import random
import datetime


def countTruePositives(target, predicted):
	"""!
	@brief Counts the number of true positives given data structures
	with predicted and target values.

	@param target (numpy.ndarray) Target values, should be binary.

	@param predicted (numpy.ndarray) Predicted values, should be binary.

	@return (int in range [0, &infin)) Number of true positives in predicted.
	"""

	return np.sum(np.where(np.logical_and(np.equal(target, np.ones(target.shape)), np.equal(predicted, np.ones(target.shape))), np.ones(target.shape), np.zeros(target.shape)))


def countTrueNegatives(target, predicted):
	"""!
	@brief Counts the number of true negatives given data structures
	with predicted and target values.

	@param target (numpy.ndarray) Target values, should be binary.

	@param predicted (numpy.ndarray) Predicted values, should be binary.

	@return (int in range [0, &infin)) Number of true negatives in predicted.
	"""

	return np.sum(np.where(np.logical_and(np.equal(target, np.zeros(target.shape)), np.equal(predicted, np.zeros(target.shape))), np.ones(target.shape), np.zeros(target.shape)))


def countFalsePositives(target, predicted):
	"""!
	@brief Counts the number of false positives given data structures
	with predicted and target values.

	@param target (numpy.ndarray) Target values, should be binary.

	@param predicted (numpy.ndarray) Predicted values, should be binary.

	@return (int in range [0, &infin)) Number of false positives in predicted.
	"""

	return np.sum(np.where(np.logical_and(np.equal(target, np.zeros(target.shape)), np.equal(predicted, np.ones(target.shape))), np.ones(target.shape), np.zeros(target.shape)))


def countFalseNegatives(target, predicted):
	"""!
	@brief Counts the number of false negatives given data structures
	with predicted and target values.

	@param target (numpy.ndarray) Target values, should be binary.

	@param predicted (numpy.ndarray) Predicted values, should be binary.

	@return (int in range [0, &infin)) Number of false negatives in predicted. 
	"""

	return np.sum(np.where(np.logical_and(np.equal(target, np.ones(target.shape)), np.equal(predicted, np.zeros(target.shape))), np.ones(target.shape), np.zeros(target.shape)))


def calculateRecall(TP, FN):
	"""!
	@brief Calculates the recall (true positive rate) of a binary
	classifier.

	@details Given the number of true positives and false negatives,
	calculates the recall of the classifier. Recall is sometimes referred
	to as sensitivity, hit rate, or true positive rate. Uses machine
	epsilon to avoid division by 0.

	@param TP (int) Number of true positives.

	@param FN (int) Number of false negatives

	@return (float in range [0,1]) Recall of the binary classifier. 
	"""

	return 1.*TP/(TP + FN + np.finfo(float).eps)


def calculateSpecificity(TN, FP):
	"""!
	@brief Calculates the specificity (true negative rate) of a binary
	classifier.

	@details Given the number of true negatives and false positives,
	calculates the specificity of the classifier. Specificity is
	sometimes referred to as the true negative rate. Uses machine
	epsilon to avoid division by 0.

	@param TP (int) Number of true negatives.

	@param FN (int) Number of false positives.

	@return (float in range [0,1]) Specificity of the binary classifier. 
	"""

	return 1.*TN/(TN + FP + np.finfo(float).eps)	


def calculatePrecision(TP, FP):
	"""!
	@brief Calculates the precision (positive predictive value) of a
	binary classifier.

	@details Given the number of true positives and false positives,
	calculates the precision of the classifier. Precison is sometimes
	referred to as the positive predictive value. Uses machine	epsilon
	to avoid division by 0.

	@param TP (int) Number of true positives.

	@param FP (int) Number of false positives.

	@return (float in range [0,1]) Precision of the binary classifier. 
	"""

	return 1.*TP/(TP + FP + np.finfo(float).eps)


def calculateNPV(TN, FN):
	"""!
	@brief Calculates the negative predictive value of a binary classifier.

	@details Given the number of true negatives and false negatives,
	calculates the negative predictive value of the classifier. Uses 
	machine epsilon to avoid division by 0.

	@param TN (int) Number of true negatives.

	@param FN (int) Number of false negatives.

	@return (float in range [0,1]) Negative predictive value of the binary classifier. 
	"""

	return 1.*TN/(TN + FN + np.finfo(float).eps)


def calculateAccuracy(TP, TN, FP, FN):
	"""!
	@brief Calculates the accuracy (Rand index) of a binary classifier.

	@details Given the contents of a classifiers confusion matrix,
	calculates the accuracy of the classifier. Accuracy is sometimes
	referred to as the Rand index. Uses machine	epsilon to avoid
	division by 0.

	@param TP (int) Number of true positives.

	@param TN (int) Number of true negatives.

	@param FP (int) Number of false positives.

	@param FN (int) Number of false negatives.

	@return (float in range [0,1]) Accuracy of the binary classifier. 
	"""

	return 1.*(TP + TN)/(TP + TN + FP + FN + np.finfo(float).eps)


def calculateF1Score(TP, TN, FP, FN):
	"""!
	@brief Calculates the F1 score of a binary classifier.

	@details Given the contents of a classifiers confusion matrix,
	calculates the F1 score of the classifier. The F1 score is a
	performance measure which is used to determine a binary classifiers
	effectiveness. It considers both the precision and recall of the 
	classifier. Uses machine epsilon to avoid division by 0.

	@param TP (int) Number of true positives.

	@param TN (int) Number of true negatives.

	@param FP (int) Number of false positives.

	@param FN (int) Number of false negatives.

	@return (float in range [0,1]) F1 score of the binary classifier. 
	"""

	p = calculatePrecision(TP, FP)
	r = calculateRecall(TP, FN)
	return 2.*(p*r)/(p + r + np.finfo(float).eps)


def calculateMCC(TP, TN, FP, FN):
	"""!
	@brief Calculates the Matthews correlation coefficient of a binary
	classifier.

	@details Given the contents of a classifiers confusion matrix,
	calculates the MCC of the classifier. The MCC considers all of the
	elements of the confusion matrix (unlike the F1 score which fails to
	consider the number of true negatives). A MCC value of +1 represents
	perfect prediction, 0 represents prediction which is no better than
	random guessing, and -1 represents completely incorrect predictions.
	Uses machine epsilon to avoid division by 0.

	@param TP (int) Number of true positives.

	@param TN (int) Number of true negatives.

	@param FP (int) Number of false positives.

	@param FN (int) Number of false negatives.

	@return (float in range [-1,1]) MCC of the binary classifier. 
	"""

	return ((TP*TN)-(FP*FN))/(np.sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN)) + np.finfo(float).eps)


def calculateInformedness(TP, TN, FP, FN):
	"""!
	@brief Calculates the informedness of a binary classifier.

	@details Given the contents of a classifiers confusion matrix,
	calculates the informedness of the classifier. Informedness is a
	component of the Matthews correlation coefficient which corresponds
	with information flow. Uses machine epsilon to avoid division by 0.

	@param TP (int) Number of true positives.

	@param TN (int) Number of true negatives.

	@param FP (int) Number of false positives.

	@param FN (int) Number of false negatives.

	@return (float in range [-1,1]) Informedness of the binary classifier. 
	"""

	return calculateRecall(TP, FN) + calculateSpecificity(TN, FP) - 1.


def calculateMarkedness(TP, TN, FP, FN):
	"""!
	@brief Calculates the markedness of a binary classifier.

	@details Given the contents of a classifiers confusion matrix,
	calculates the markedness of the classifier. Markedness is a
	component of the Matthews correlation coefficient which corresponds
	with information flow and is the inverse of informedness. Uses
	machine epsilon to avoid division by 0.

	@param TP (int) Number of true positives.

	@param TN (int) Number of true negatives.

	@param FP (int) Number of false positives.

	@param FN (int) Number of false negatives.

	@return (float in range [-1,1]) Markedness of the binary classifier. 
	"""

	return calculatePrecision(TP, FP) + calculateNPV(TN, FN) - 1.


if __name__ == '__main__':
	with open('../Data/modelTestOutput.pkl', 'rb') as inFile:
		data, target, output = pickle.load(inFile)

	outfile = open('../Visualizations/Confusion/BinaryAnalysisInfo.log', 'w')

	outfile.write('Timestamp: ' + str(datetime.datetime.now()) + '\n\n\n')

	binData = np.where(data > np.mean(data), np.ones(data.shape), np.zeros(data.shape))
	binTarget = np.where(target > np.mean(target), np.ones(target.shape), np.zeros(target.shape))
	binOutput = np.where(output > np.mean(output), np.ones(output.shape), np.zeros(output.shape))

	for i in range(binData.shape[0]-1):
		plotData = [[data[i,0,:,:], target[i,0,:,:], output[i,0,:,:]],
					[binData[i,0,:,:], binTarget[i,0,:,:], binOutput[i,0,:,:]]]

		TP = countTruePositives(binTarget[i,0,:,:], binOutput[i,0,:,:])
		TN = countTrueNegatives(binTarget[i,0,:,:], binOutput[i,0,:,:])
		FP = countFalsePositives(binTarget[i,0,:,:], binOutput[i,0,:,:])
		FN = countFalseNegatives(binTarget[i,0,:,:], binOutput[i,0,:,:])

		# Need to clean up this output string
		outfile.write('Image Number:              %i \nTrue Positives:            %i \nTrue Negatives:            %i \nFalse Positives (Type I):  %i \nFalse Negatives (Type II): %i \nAccuracy:                  %f \nF1 Score:                  %f \nMatthews Corr. Coeff.:     %f \nInformedness:              %f \nMarkedness:                %f \n\n'%(i, TP, TN, FP, FN, calculateAccuracy(TP, TN, FP, FN), calculateF1Score(TP, TN, FP, FN), calculateMCC(TP, TN, FP, FN), calculateInformedness(TP, TN, FP, FN), calculateMarkedness(TP, TN, FP, FN)))

		# Plot tiled images to allow for visual inspection.
		fig, ax = plt.subplots(2, 3)

		for j in range(ax.shape[0]):
			for k in range(ax.shape[1]):
				ax[j,k].imshow(plotData[j][k], cmap=plt.get_cmap('Greys'), interpolation="nearest")

				# Don't draw the axes
				ax[j,k].set_frame_on(False)

				# Remove the ticks
				ax[j,k].set_xticks([]) 
				ax[j,k].set_yticks([])

				# Create labels and titles
				if j == 0 and k == 0:
					ax[j,k].set_ylabel('Original\nImages', labelpad=30, rotation=0)
					ax[j,k].set_title('Input')

				elif j == 0 and k == 1:
					ax[j,k].set_title('Target')

				elif j == 0 and k == 2:
					ax[j,k].set_title('Output')

				elif j == 1 and k == 0:
					ax[j,k].set_ylabel('Binarized\nImages', labelpad=30, rotation=0)

				else:
					pass
		
		fig.savefig('../Visualizations/Confusion/BinaryAnalysis%i.png'%(i), bbox_inches='tight')
		fig.savefig('../Visualizations/Confusion/BinaryAnalysis%i.pdf'%(i), bbox_inches='tight')
		plt.close(fig)

	outfile.close()