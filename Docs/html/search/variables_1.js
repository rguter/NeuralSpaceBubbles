var searchData=
[
  ['bbox_5finches',['bbox_inches',['../namespaceoutputAnalysis.html#a0aa57426fdc0836049074002c830984c',1,'outputAnalysis']]],
  ['beta',['Beta',['../classnetworkLayers_1_1BatchNormalizationLayer.html#a3f11a5f64ec3e7c32635c66d8f3167e4',1,'networkLayers.BatchNormalizationLayer.Beta()'],['../classnetworkLayers_1_1CAEConvLayer.html#a3b5b0cb8659487241b59f92c56e38885',1,'networkLayers.CAEConvLayer.Beta()'],['../classnetworkLayers_1_1CAEDeconvLayer.html#aff0e3d332851ff4c36ff10c428648171',1,'networkLayers.CAEDeconvLayer.Beta()'],['../classnetworkLayers_1_1DenseLayer.html#a9cec99be8c25e8803829a77d3a892e04',1,'networkLayers.DenseLayer.Beta()']]],
  ['bindata',['binData',['../namespaceoutputAnalysis.html#afaecd02ecd9e910cb669c12e1a40c628',1,'outputAnalysis']]],
  ['binoutput',['binOutput',['../namespaceoutputAnalysis.html#a7b8d6540938355f557b48d989d75dfe1',1,'outputAnalysis']]],
  ['bintarget',['binTarget',['../namespaceoutputAnalysis.html#a07c29efe4c397cd297aa1430bef28c53',1,'outputAnalysis']]]
];
