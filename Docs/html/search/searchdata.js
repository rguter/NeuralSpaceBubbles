var indexSectionsWithContent =
{
  0: "_abcdefgilmnoprstw",
  1: "bcdfmrs",
  2: "acdnop",
  3: "acdnopr",
  4: "_abcdelmprst",
  5: "abcdefgiloprstw",
  6: "n"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

