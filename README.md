# NeuralSpaceBubbles

---

## [Documentation](https://omega1563.gitlab.io/NeuralSpaceBubbles)

---

## Description
The goal of this project is to produce an unsupervised (or semi-supervised) machine learning method for identifying radiation driven shells in space telescope images of molecular clouds.

Currently a convolutional auto-encoder is trained to identify radiation driven shells in tagged simulation data, and has obtained as low as 12% relative error (averaged over the pixels of each image and each image in a testing set).

Next steps include streamlining the code base, addition and improvement of documentation, implementation of experiments and metrics to measure model performance, implementation of advanced feature to improve model performance, and more.

---

## Contents
 * Data: Contains the data used to train and test radiation driven shell identifiers.
  * bubbleDataset.pkl: The reformatted and augmented dataset which is fed into our neural network.
  * density.pkl: The density data, after it has been sliced into 2-D structures.
  * densityAug.pkl: The density data, after it has been sliced and augmented.
  * modelTestOutput.pkl: Data from the testing set, corresponding targets, and output data from the trained network.
  * tracer.pkl: The tracer data (corresponding targets to the density data), after it has been sliced into 2-D structures.
  * tracerAug.pkl: The tracer data, after it has been sliced and augmented.
 * Source: Contains the project source code.
  * LegacySource: Contains legacy source code.
  * augmentedDataVis.py: Creates a visualization of the data after preprocessing.
  * cae.py: A convolutional auto-encoder model which was designed to train on simulation data and identify radiation driven shells in a cloud of gas.
  * dataPreparation.py: Performs all of the data pre-processing and formatting required for the CAE model.
  * networkLayers.py: Contains several layer objects used to construct the convoluational auto-encoder model.
  * parameterSweep.py: Automates the process of searching the hyper-parameter space using a random search method. Outputs information at runtime and saves the best model.
 * Visualizations: Contains all of the visualizations created by and/or for our models.
 * README.md: Describes this project and it's file structure.

---

## Dependencies
 * [Python](https://docs.python.org/3/) (tested with version 3.5)
 * [Theano](http://deeplearning.net/software/theano/) (tested with version 0.8.2)
 * [Numpy](http://www.numpy.org/) (tested with version 1.11.0)
 * [Astropy](http://www.astropy.org/) (tested with version 1.1.2)
 * [Matplotlib](http://matplotlib.org/) (tested with version 1.5.2)

---
